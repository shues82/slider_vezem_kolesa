import React from 'react';
import './App.css';
import { Slider } from './slider';
import { Banner } from './banner';

function App() {
  const fons = 'assets/fons_for_slider/';
  const items = [
    {
      type: 'template',
      content: {
        title: 'Шины и диски',
        titleColor: 'white',
        titleBackground: '#2C5692',
        subtitle: 'для любых автомобилей<br/> по лучшим ценам',
        picture: fons + 'shiny.png',
      },
    },
    {
      type: 'template',
      content: {
        title: 'Бесплатная',
        titleColor: 'white',
        titleBackground: '#21A038',
        subtitle: 'эвакуация в случае проблем<br/> с шинами для покупателей',
        picture: fons + 'evacuation.png',
      },
    },
    {
      type: 'template',
      content: {
        title: 'Заправься',
        titleColor: 'white',
        titleBackground: '#D71E58',
        subtitle: '<span>бесплатно с шинами<br/> Michelin</span>',
        picture: fons + 'fill.png',
      },
    },
    {
      type: 'template',
      content: {
        title: 'Скидка -30%',
        titleColor: '#444444',
        titleBackground: '#FAFF19',
        subtitle: 'для Вас на хранение<br/> шин круглый год',
        picture: fons + 'sale.png',
      },
    },
    {
      type: 'template',
      content: {
        title: 'Шиномонтаж',
        titleColor: 'white',
        titleBackground: '#444444',
        subtitle: 'бесплатно для покупателей<br/> комплекта!',
        picture: fons + 'shinomontazh.png',
      },
    },
  ];

  return (
    <div className="App">
      <Slider data={items} speed={2000} size={1} />
    </div>
  );
}

export default App;
