import React from 'react';
import './index.css';

export function Controls(props) {
    function shift(direction){
        props.setMode(direction)
    }
  return (
    <div className="controls">
      <div className="controlButton scroll left" onClick={()=>shift("left")}><div className='controlButtonCaption'>{'<'}</div></div>

      <div className="controlButton scroll right" onClick={()=>shift("right")}><div className='controlButtonCaption'>{'>'}</div></div>
    </div>
  );
}
