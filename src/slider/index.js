import React from 'react';
import './index.css';
import { Item } from '../item';
import { Controls } from '../controls';

export function Slider(props) {
  const [mode, setMode] = React.useState('pause');

  const [timerId, setTimerId] = React.useState(null);

  const [sliderStructure, setSliderStructure] = React.useState({
    leftPhantomIndex: null,
    rightPhantomIndex: null,
    showed: [],
  });

  React.useEffect(() => {
    if (!initialized()) {
      firstInit();
    }
  }, [props.size]);

  React.useEffect(() => {
    // console.log(mode);
    switch (mode) {
      case 'right':
      case 'left':
        clearTimeout(timerId);
        setMode('animating_' + mode);
        animate(timing, draw, props.speed);
        break;
      case 'animated_left':
      case 'animated_right':
        clearDraw();
        updateStructure(clearDirection(mode));

        clearTimeout(timerId);

        const bufId = setTimeout(() => {
          // console.log(mode.split('_')[0]);
          if (mode.split('_')[0] === 'animated') setMode('right');
        }, 3000);

        setTimerId(bufId);
        break;
      default:
        break;
    }
  }, [mode]);

  function updateStructure(direction) {
    const newStructure = {};
    newStructure.leftPhantomIndex = newPhantomIndex(
      sliderStructure.leftPhantomIndex,
      direction
    );
    newStructure.rightPhantomIndex = newPhantomIndex(
      sliderStructure.rightPhantomIndex,
      direction
    );
    newStructure.showed = [
      ...loadShowedByPhantomIndexes(
        newStructure.leftPhantomIndex,
        newStructure.rightPhantomIndex
      ),
    ];
    setSliderStructure(newStructure);
    setMode('pause');
  }

  function clearDirection(val) {
    return val.split('_')[1];
  }

  function clearDraw() {
    document.querySelector('.slider').children[0].style.marginLeft = 0;
    document.querySelector('.slider').children[0].style.marginRight = 0;
  }

  function newPhantomIndex(index, direction) {
    switch (direction) {
      case 'right':
        index++;
        if (index >= bufer.length) {
          index = 0;
        }
        break;
      case 'left':
        index--;
        if (index < 0) {
          index = bufer.length - 1;
        }
        break;
      default:
        break;
    }
    return index;
  }

  function loadShowedByPhantomIndexes(leftIndex, rightIndex) {
    const showed = [];
    if (leftIndex < rightIndex) {
      for (let i = leftIndex + 1; i < rightIndex; i++) {
        showed.push(i);
      }
    } else {
      for (let i = leftIndex + 1; i < bufer.length; i++) {
        showed.push(i);
      }
      for (let i = 0; i < rightIndex; i++) {
        showed.push(i);
      }
    }
    return showed;
  }

  function initialized() {
    if (sliderStructure.leftPhantomIndex !== null) return true;
    return false;
  }

  function firstInit() {
    const newStructure = {};
    newStructure.leftPhantomIndex = props.data.length - 1;
    newStructure.rightPhantomIndex = props.size;
    newStructure.showed = [
      ...loadShowedByPhantomIndexes(
        newStructure.leftPhantomIndex,
        newStructure.rightPhantomIndex
      ),
    ];
    setSliderStructure(newStructure);
    const bufId = setTimeout(() => setMode('right'), 5000);
    setTimerId(bufId);
  }

  function timing(timeFraction) {
    return Math.pow(timeFraction, 2);
  }

  function draw(progress) {
    const elem = document.querySelector('.slider').children[0];
    // const direction = mode.split('_')[1];
    // console.log(mode);
    if (mode === 'right') {
      elem.style.marginLeft =
        -(progress * 100 * (elem.scrollWidth / 100)) + 'px';
    } else {
      elem.style.marginLeft = progress * 100 * (elem.scrollWidth / 100) + 'px';
    }
  }

  function animate(timing, draw, duration) {
    let start = performance.now();

    requestAnimationFrame(function animate(time) {
      let timeFraction = (time - start) / duration;
      if (timeFraction > 1) {
        timeFraction = 1;
        setMode('animated_' + mode);
      }

      let progress = timing(timeFraction);

      draw(progress);

      if (timeFraction < 1) {
        requestAnimationFrame(animate);
      }
    });
  }

  function combineItems() {
    const items = [bufer[sliderStructure.leftPhantomIndex]];
    sliderStructure.showed.forEach((el) => {
      items.push(bufer[el]);
    });
    items.push(bufer[sliderStructure.rightPhantomIndex]);
    return items;
  }

  function controlAction(action){
    // console.log(action);
    if(mode.split('_')[0]==="animating"){
      return;
    }
    setMode(action);
  }

  const bufer = props.data.map((el, i) => <Item data={el.content} key={i} />);

  return (
    <div className="container" onMouseOver={() => {}} onMouseOut={() => {}}>
      <div className="slider">{combineItems()}</div>
      <Controls mode={mode} setMode={controlAction} />
    </div>
  );
}
