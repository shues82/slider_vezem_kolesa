import React from 'react';
import './index.css';

export function Banner(props) {
  return (
    <div className="banner">
      <div className="polygon">
        <div className="promo">
          <div className="bay">
            <div className="badge">
              <div className='dot'></div>
              {/* <div className="nose"></div> */}
              <div className="sberLogo"></div>
              <div className="bage_caption">Покупай</div>
            </div>
            <div className="subBage">со Сбером</div>
          </div>
          <div className="credit">
            АВТОТОВАРЫ МОЖНО
            <br />
            КУПИТЬ В РАССРОЧКУ
          </div>
          <div className="more">
            <div className="button_more">Узнать подробнее</div>
          </div>
          <div className="smallText">
            ПАО СберБанк. «Потребительский кредит на покупку в
            интернет-магазинах». Генеральная лицензия Банка России на
            осуществление банковских операций №1481 от 11.08.2015. Рассрочка –
            приобретение товара/услуги за счет предоставления Партнером Банка
            (продавцом) скидки на товар/услугу. Увеличение затрат не происходит
            только в случае надлежащего исполнения заемщиком своих обязательств
            по кредитному договору.
          </div>
        </div>
      </div>
      <div className="polygon_1"></div>
      <div className="banner_imgWrapper"></div>
    </div>
  );
}
