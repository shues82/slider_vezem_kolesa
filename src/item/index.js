import React from 'react';
import './index.css';

export function Item(props) {
  const data = props.data;
  const imgUrl = data.picture;
  const titleContainerBackground = data.titleBackground;
  return (
    <div className="item">
      <div className="itemContent">
        <div className="textSection">
          <div className="blanckBlock"></div>
          <div
            className="titleContainer"
            style={{ backgroundColor: `${ titleContainerBackground }`, color: `${data.titleColor}` }}
          >
            {data.title}
          </div>
          <div className="subtitleContainer" dangerouslySetInnerHTML={{__html: data.subtitle}} ></div>
        </div>
        <div className="imgSection">
          <div className="imgTemplate">
            <div
              className="imgWrapper"
              style={{ backgroundImage: `url(${imgUrl})` }}
            ></div>
          </div>
        </div>
      </div>
    </div>
  );
}
